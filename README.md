# Here is a light example without JSX

```javascript
render() {
		return el('form', {
			className: css.login,
			onChange: this.onChange.bind(this),
			onSubmit: this.onSubmit.bind(this)
		}, ...[
			el('input', {
				placeholder: 'username',
				type: 'text',
				name: 'username'
			}),
			el('input', {
				placeholder: 'password',
				type: 'password',
				name: 'password'
			}),
			el('button')
		]);
	}
}
```

# Its not horrible at this level but still I think the JSX equivalent is way easier on the eye:

```javascript
render() {
	return (
		<form className={css.login} onChange={this.onChange} onSubmit={this.onSubmit}>
			<input name="username" placeholder="Username"/>
			<input name="password" placeholder="Password" type="password"/>
			<button />
		</form>
	);
}
```

# My Vue Template;

```html
<template>
	<form :class="this.css.login" @change="this.onChange" @submit="this.onSubmit">
		<input name="username" placeholder="Username"/>
		<input name="password" placeholder="Password" type="password"/>
		<button/>
	</form>
<template>
```
